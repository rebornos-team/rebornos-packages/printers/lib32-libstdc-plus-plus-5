# lib32-libstdc plus plus 5

Legacy GNU Standard C++ library version 3 (32 bit)

http://gcc.gnu.org

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-packages/printers/lib32-libstdc-plus-plus-5.git
```

